famous_person = "   Winston Churchill      "

message = f'{famous_person} zij ooit: \n\t"If you are going throught hell, keep going!"'

print(message)

# Ik dacht dat je hier famous_person.lstrip() kon doen en daarna de message weer opnieuw kon printen, dit blijkt niet te kunnen :(

message = f'{famous_person.lstrip()} zij ooit: \n\t"If you are going throught hell, keep going!"'

print(message)

message = f'{famous_person.rstrip()} zij ooit: \n\t"If you are going throught hell, keep going!"'

print(message)

message = f'{famous_person.strip()} zij ooit: \n\t"If you are going throught hell, keep going!"'

print(message)