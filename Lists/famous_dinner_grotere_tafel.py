famous = ["Bert Visscher", "Jochem Myer", "David Attenborough"]

count = 0

for i in famous:
    print("He " + famous[count] + " kom je bij me eten.")
    count += 1

print("Ik hoor net dat " + famous.pop(1) + " niet kan komen. \n")

famous.insert(1, "Marco Borsato")

count2 = 0

for i in famous:
    print("He " + famous[count2] + " jij bent echt uitgenodigd.")
    count2 += 1

print("Oh Damn! We hebben een grotere tafel. \n")

famous.insert(0, "Chantal Janzen")
famous.insert(2, "Barack Obama")
famous.append("Linda de Mol")

count3 = 0

for i in famous:
    print("Jij bent hierbij echt echt uitgenodigd " + famous[count3] + ". Cool he :)")
    count3 += 1
