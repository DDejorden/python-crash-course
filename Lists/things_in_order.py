mtgCardTypes = ['Lnds', 'Instant', 'Sorcery']

print(mtgCardTypes)

mtgCardTypes[0] = 'Lands'

mtgCardTypes.append('Planeswalker')

print(mtgCardTypes)

mtgCardTypes.insert(2, 'Creature')

mtgCardTypes.insert(2, 'Sike!')

print(mtgCardTypes)

del mtgCardTypes[2]

mtgCardTypes.append('Homonculus')

print(mtgCardTypes)

popedCard = mtgCardTypes.pop()

print('Hé, ' + popedCard + ' is geen card type! Die moet weg.')

print(mtgCardTypes)

mtgCardTypes.append('Egyption god card')

print(mtgCardTypes)

mtgCardTypes.append('Artifact')

mtgCardTypes.append('Enchantment')

mtgCardTypes.remove('Egyption god card')

print(mtgCardTypes)

print('Zo nu wordt het tijd om ze te sorteren.')

print(sorted(mtgCardTypes))

print(sorted(mtgCardTypes, reverse=True))

mtgCardTypes.sort(reverse=True)

print(mtgCardTypes)

mtgCardTypes.sort()

print(mtgCardTypes)

mtgCardTypes.reverse()

print(mtgCardTypes)

print('In totaal zijn er ' + str(len(mtgCardTypes)) + ' types Magic kaarten.')
